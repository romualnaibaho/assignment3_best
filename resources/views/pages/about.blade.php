<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>BEST</title>
</head>
<body>
  <h1>This is {{ $pageName }}</h1>

  <form action="{{ route('add-keyboard') }}" method="POST">
    @csrf
    <input type="text" name="merk" required placeholder="Merk">
    <button type="submit">Add Kerboard</button>
  </form>

  <ul>
    @foreach ($keyboardData as $item)
      <li>
        {{ "$item->code - $item->merk" }}
         - <a href="{{ route('delete-keyboard', ['id' => $item->id]) }}">Delete</a>
         - <a href="{{ route('edit-keyboard', ['id' => $item->id]) }}">Edit</a>
      </li>
    @endforeach
  </ul>
</body>
</html>
