import Home from '../src/pages/Home.vue';
import About from '../src/pages/About.vue';

const BookList = () => import('../src/pages/List.vue')
const BookCreate = () => import('../src/pages/Add.vue')
const BookEdit = () => import('../src/pages/Edit.vue')


const routes = [
    {
        path: '/',
        component: Home,
        name: 'home'
    },
    {
        path: '/about',
        component: About,
        name: 'about'
    },
    {
        path: '/book',
        component: BookList,
        name: 'bookList'
    },
    {
        path: '/book/:id/edit',
        component: BookEdit,
        name: 'bookEdit'
    },
    {
        path: '/book/add',
        component: BookCreate,
        name: 'bookAdd'
    }
]

export default routes;
