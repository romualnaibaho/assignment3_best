<?php

namespace App\Http\Controllers;

use App\Models\Keyboard;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    public function home()
    {
      return "Home Controller";
    }

    public function about()
    {
      $keyboard = Keyboard::all();

      return view('pages.about', [
        'pageName' => 'FIGAR',
        'keyboardData' => $keyboard
      ]);
    }

    public function addKeyboard(Request $request)
    {
      $keyboard = new Keyboard();
      $keyboard->code = Str::random(5);
      $keyboard->merk = $request->merk;

      $keyboard->save();

      return redirect(route('about-page'));
    }

    public function editKeyboard($id)
    {
      $keyboard = Keyboard::find($id);

      return view('pages.edit_keyboard', [
        'keyboardData' => $keyboard
      ]);
    }

    public function updateKeyboard(Request $request)
    {
      $keyboard = Keyboard::find($request->id);
      $keyboard->merk = $request->merk;

      $keyboard->save();

      return redirect(route('about-page'));
    }

    public function deleteKeyboard($id)
    {
      $keyboard = Keyboard::find($id);
      $keyboard->delete();

      return redirect(route('about-page'));
    }
}
