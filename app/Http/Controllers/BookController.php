<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function index()
    {
        $books = Book::all(['id','title','description']);
        return response()->json($books);
    }

    public function store(Request $request)
    {
        $book = Book::create($request->post());
        return response()->json([
            'message'=>'Book Created Successfully!!',
            'book'=>$book
        ]);
    }

    public function show(Book $book)
    {
        return response()->json($book);
    }

    public function update(Request $request, Book $book)
    {
        $book->fill($request->post())->save();
        return response()->json([
            'message'=>'Book Updated Successfully!!',
            'book'=>$book
        ]);
    }

    public function destroy(Book $book)
    {
        $book->delete();
        return response()->json([
            'message'=>'Book Deleted Successfully!!'
        ]);
    }
}
