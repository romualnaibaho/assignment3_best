<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', [HomeController::class, 'home']);
// Route::get('/about', [HomeController::class, 'about'])->name('about-page');

// Route::post('/add-keyboard', [HomeController::class, 'addKeyboard'])->name('add-keyboard');
// Route::get('/edit-keyboard/{id}', [HomeController::class, 'editKeyboard'])->name('edit-keyboard');
// Route::get('/delete-keyboard/{id}', [HomeController::class, 'deleteKeyboard'])->name('delete-keyboard');
// Route::post('/update-keyboard', [HomeController::class, 'updateKeyboard'])->name('update-keyboard');

Route::get('{any}', function () {
    return view('app');
})->where('any', '.*');